#!/bin/bash
# wts-rename.sh 20200504
# This script is licensed under the GNU Affero General Public License v3.0
# More information at https://gitlab.com/bashgordon/farnsworths-finglongers

### INIT
filelist=""
verbosity=0
force=0
help="wts-rename.sh\nUsage: wts-rename.sh -Options [FILE1 FILE2 ... FILE]\nWildcards allowed: wts-rename.sh -Options [*.*]\n\nOptions\n-f          Force rename\n            wts-rename.sh will rename files only when forced to\n-h          Display this help message\n-v          Output operations to stdout\n"

if [ $# -eq 0 ]; then echo -e "**ERROR: Not enough arguments.\n\n$help"; exit 1; fi
for a in $@; do
    case "$a" in
        -h) echo -e "$help"; exit 0;;
        -v) verbosity=1;;
        -f) force=1;;
        *)  filelist=$filelist" "$(echo "$a" |awk '/\<.*_[0-9]{2}-[0-9]{2}-[0-9]{4}_.*\>/ { print $1 }');;
        esac
    done

### MAIN LOOP
i=0

for source in $filelist; do
    dest=$(echo $source |awk 'BEGIN { FS="-" } {print substr($1,1,length($1)-2) substr($3,1,4) "-" substr($2,1) "-" substr($1,length($1)-1) substr($3,5)}')
    if [ $verbosity -eq 1 ] || [ $force -eq 0 ]; then printf "**INFO: $source >> $dest\n"; fi
    if [ $force -eq 1 ]; then mv "$source" "$dest"; fi
    let "i++"
    done
if [ $verbosity -eq 1 ] || [ $force -eq 0 ]; then printf "$i files processed.\n"; fi
if [ $force -eq 0 ]; then printf "**WARNING: This was a dry-run. No files have been renamed. Use -f to force renaming.\n"; fi
exit 0