#!/bin/bash
# imgcd.sh 20201221
# This script is licensed under the GNU Affero General Public License v3.0
# More information at https://gitlab.com/bashgordon/farnsworths-finglongers

# INIT
bs=$(cdinfo -d -i /dev/cdrom | grep -i 'block size' | awk-F ": " '{print $2}');
vs=$(cdinfo -d -i /dev/cdrom | grep -i 'volume size' | awk-F ": " '{print $2}');
vn=$(cdinfo -d -i /dev/cdrom | grep -i 'volume id' | awk-F ": " '{print $2}');

# MAIN LOOP
umount /dev/cdrom;
dd if=/dev/cdrom of=$1$vn.img bs=$bs count=$vs status=progress;